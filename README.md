## SmartPay

### Watch Video [here](https://drive.google.com/file/d/1cbYlks0F12Wvl-D5AWgOKhf6wxn20btB)

## For Development

Pull the repository
```console
git clone https://github.com/daviddl9/SmartPay.git
```

1. In Chrome browser, navigate to **[Chrome Extensions](chrome://extensions/)**.  
2. Enable **Developer mode**  
3. Select **Load unpacked** and select the repository.  
 
![alt text](extension.png)
